# code_guesser.sh

A little game to test how good did you know your code.

`code_guesser.sh` contains following five levels:

* level1 -> your see only the outline of your code like:
  
    ```c
    XXXXXXXXXXXXXXXXXX

    XXXXXXXXXX
    X
    XXXXXXXXXXXXXXXXXXXXXXXXXX
    XXXXXXXXXXXXX
    X
    ```

* level2 -> your can see the spaces of your code like:
  
    ```c
    XXXXXXXX XXXXXXXXX

    XXX XXXXXX
    X
        XXXXXXXXXXXXX XXXXXXXX
        XXXXXX XX
    X
    ```

* level3 -> your can see the spaces and non-alphabetical character of your code like:
  
    ```c
    XXXXXXXX XXXXXXXXX

    XXX XXXXXX
    X
        XXXXXXXXXXXXX XXXXXXXX
        XXXXXX XX
    X
    ```

* level4 -> you can see the spaces, non-alphabetical characters and keywords of your code like:

    ```c
    #include <stdio.h>

    int xxxx()
    {
        xxxxxx("xxxxx xxxxx");
        return 0;
    }
    ```
  
* level5 -> you can see the spaces, non-alphabetical characters, keywords and upper characters of your code like:

    ```c
    #include <stdio.h>

    int xxxx()
    {
        xxxxxx("Hxxxx Wxxxx");
        return 0;
    }
    ```

* at last the solution -> you can see the spaces, non-alphabetical characters, keywords and upper characters of your code like:

    ```c
    #include <stdio.h>

    int main()
    {
        printf("Hello World");
        return 0;
    }
    ```

## Usage

```txt
Usage: code_guesser.sh <FILE_PATH> [CUSTOM_STOP_WORD_FILE]... [OPTION]...

OPTION:

    ** stop word lists (if no one is given, a stop word list based on file extension will be used)
    --no-defaults - disable default stop word list
    --cs - use default stop word list for csharp
    --c - use default stop word list for c
    --cpp - use default stop word list for cpp
    --js - use default stop word list for javascript
    --java - use default stop word list for java
    --rs - use default stop word list for rust
    --xml - use default stop word list for xml
    --all - use all default stop word lists
    
    ** in/output settings
    --quiet - do not print additional output like level number
    --non-interactive - disable user interaction like read and clear

    ** all five level will be printed by default, except one of following options is used
    --level1 - show level 1
    --level2 - show level 2
    --level3 - show level 3
    --level4 - show level 4
    --level5 - show level 5
    --skip-solution - do not show the solution
```

## Example

```shell
# play an example c file
src/code_guesser.sh examples/example.c

# play an example c file only level 3, 4, and 5
src/code_guesser.sh examples/example.c --level3 --level4 --level5

# pipe file content to code guesser. user interactions are disabled
cat examples/example.c | src/code_guesser.sh

# use a custom stop word list exclusive (without default stop word list)
src/code_guesser.sh  examples/example.c examples/custom.stopwordlist --no-defaults
```

## Custom Stopword List File

A custom word list file contains regex expressions like:

```regex
\bHello\b
#include .*
```

Each line is new regex expression. It regex extension  of `gnu sed` is used (<https://www.gnu.org/software/sed/manual/html_node/regexp-extensions.html>)

## Packages

### Nix Package

```shell
# build nix package
nix-build pkgs/nix/

# use script from package
result/bin/code_guesser.sh examples/example.c

# run code guesser inside a pure nix shell
nix-shell pkgs/nix/shell.nix --pure --run "code_guesser.sh  examples/example.c"
```

### Docker

```shell
# local docker build
nix-build pkgs/docker && docker load -i result 

# run code guesser inside a docker container (file are mounted as volume)
docker run --rm -ti -v $PWD/examples:/examples code_guesser.sh "examples/example.c"

# pipe a file to code guesser docker container
cat examples/example.c | docker run --rm -i code_guesser.sh

# also can directly pulled from hub.docker.com
docker pull stubbfel/code_guesser.sh
docker run --rm -ti -v $PWD/examples:/examples stubbfel/code_guesser.sh "examples/example.c"
cat examples/example.c | docker run --rm -i stubbfel/code_guesser.sh
```

### Flake

```shell
# nix flake are experimental and required following options --experimental-features 'nix-command flakes'
# create alias for nix --experimental-features 'nix-command flakes'
alias nixe && echo "Alias for nixe exists" || alias nixe="nix --experimental-features 'nix-command flakes'"

# run local script
nixe run ./pkgs/nix examples/example.c

# run public released script
nixe run "gitlab:codeguesser/code_guesser.sh/main?dir=pkgs/nix" examples/example.c

# run local docker image from local flake
nixe run ./pkgs/nix#dockerLocalApp examples/example.c

# run local docker image from remote flake
nixe run "gitlab:codeguesser/code_guesser.sh/main?dir=pkgs/nix#dockerLocalApp" examples/example.c

# run dockerHub image from local flake
nixe run ./pkgs/nix#dockerHubApp examples/example.c

# run dockerHub image from public flake
nixe run "gitlab:codeguesser/code_guesser.sh/main?dir=pkgs/nix#dockerHubApp" examples/example.c

# update flake lock
cd pkgs/nix && nixe run .#updateLocksApp
```