# nix-build  --expr 'with import <nixpkgs> {}; callPackage ./default.nix {}'
{
  pkgs ? import <nixpkgs> {},
  repoPath ? ../..
}:

let

src = pkgs.callPackage "${repoPath}/pkgs/nix" {};
 
in

pkgs.dockerTools.buildImage {
  name = "code_guesser.sh";
  tag = "latest";
  contents = src.codeGuesser;
  config.Entrypoint = [ "${src.codeGuesser}/bin/code_guesser.sh" ];
}