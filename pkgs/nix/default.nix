# nix-build  --expr 'with import <nixpkgs> {}; callPackage ./default.nix {}'
{
  pkgs ? import <nixpkgs> {},
  srcFolderPath ? ../../src
}:

let

srcFile = builtins.readFile "${srcFolderPath}/code_guesser.sh";

targetFile = pkgs.writeShellApplication {
    name = "code_guesser.sh";
    runtimeInputs = [ 
        pkgs.bash
        pkgs.gnused
        pkgs.coreutils-full
    ];

    text =  srcFile;
};
 
in

{

codeGuesser = targetFile;

meta = {
    description = "A little game to test, how good did you know your code.";
    homepage = https://gitlab.com/codeguesser/code_guesser.sh;
    maintainers = "stubbfel";
    license = pkgs.lib.licenses.mit;
    platforms = pkgs.lib.platforms.unix;
  };
}