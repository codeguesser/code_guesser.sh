{
  description = "code_guesser.sh flake";
  inputs.nixpkgs.url = "nixpkgs/nixos-22.05-small";
  outputs = { self, nixpkgs }:
    let
      #supportedSystems = [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];
      supportedSystems = [ "x86_64-linux" ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      nixpkgsFor = forAllSystems (system: import nixpkgs { inherit system; });
    in
    rec {
      packages = forAllSystems (system:
        {
          default = (nixpkgsFor.${system}.callPackage ./default.nix { }).codeGuesser;
          dockerLocalImage = nixpkgsFor.${system}.callPackage ./../docker { };
        });

      apps = forAllSystems (system:
        let
          pkgs = nixpkgsFor.${system};
          package = packages.${system}.default;
          buildRunScript = containerName: ''
            if [[ $# -eq 0 ]] ; then
              docker run --rm -i ${containerName} 
              exit 0
            fi

            FILE=$(realpath "''${1}")
            shift
            docker run --rm -ti -v "''${FILE}":"''${FILE}" ${containerName} "''${FILE}" "$@"
          '';

          dockerHubAppRunScript = pkgs.writeShellScriptBin "docker_hub_code_guesser.sh" (buildRunScript "stubbfel/code_guesser.sh");
          dockerLocalAppRunScript = pkgs.writeShellScriptBin "docker_local_code_guesser.sh" ''
            docker load < ${packages.${system}.dockerLocalImage}

            ${buildRunScript "code_guesser.sh"}        
          '';

          updateLockScript = pkgs.writeShellScriptBin "update_flake_lock.sh" ''
            nix --experimental-features 'nix-command flakes' flake lock --update-input nixpkgs
            nix --experimental-features 'nix-command flakes' build 
          '';
        in
        {
          default = { type = "app"; program = "${package}/bin/code_guesser.sh"; };
          dockerHubApp = { type = "app"; program = "${dockerHubAppRunScript}/bin/docker_hub_code_guesser.sh"; };
          dockerLocalApp = { type = "app"; program = "${dockerLocalAppRunScript}/bin/docker_local_code_guesser.sh"; };
          updateLocksApp = { type = "app"; program = "${updateLockScript}/bin/update_flake_lock.sh"; };
        });

      devShells = forAllSystems (system:
        {
          default = nixpkgsFor.${system}.callPackage ./shell.nix { };
        });

      hydraJobs = {
        tarball = nixpkgsFor.x86_64-linux.releaseTools.sourceTarball rec {
          name = "code_guesser.sh";
          src = builtins.filterSource (path: type: type != "directory" || baseNameOf path != ".git") ../..;
          version = "1.2.0";
          officialRelease = true;
          bootstrapBuildInputs = [ ];
          distPhase = ''
            mkdir $out/tarballs
            tar -czvf $out/tarballs/${name}-${version}.tar.gz *
          '';
        };

        runCommandHook = {
          recurseForDerivations = { };
          publishImages = forAllSystems (system:
            nixpkgsFor.${system}.writeScript "publish_docker_images.sh" ''
              #!${nixpkgsFor.${system}.runtimeShell}

              ${nixpkgsFor.${system}.docker}/bin/docker load < ${packages.${system}.dockerLocalImage}

              ${nixpkgsFor.${system}.docker}/bin/docker tag code_guesser.sh stubbfel/code_guesser.sh:latest  
              
              ${nixpkgsFor.${system}.docker}/bin/docker push stubbfel/code_guesser.sh:latest  
            ''
          );
        };
      };
    };
}
