{ pkgs ? import <nixpkgs> {}}:
let

packages = pkgs.callPackage ./default.nix {};

in

pkgs.mkShell {
    name = "codeguesser-shell";
    packages=[
        packages.codeGuesser
    ];
    shellHook = ''
        alias nixe="nix --experimental-features 'nix-command flakes'"
    '';
}

