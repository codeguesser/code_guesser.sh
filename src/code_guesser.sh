#!/bin/env bash

STOP_WORD_LINE_MARKER="123471142"

# shellcheck disable=SC2034
STOP_WORD_MARKER="${STOP_WORD_LINE_MARKER} "

DEFAULT_C_STOP_WORD_ARRAY=(
    "\bint\b"
    "\breturn\b"
    "\bvoid\b"
    "#include .*"
    "#pragma .*"
    "#define .*"
    "#if .*"
    "#elif .*"
    "\b#endif\b"
    "#ifdef .*"
    "#ifndef .*"
    "#undef .*"
    "#line .*"
    "#error .*"
    "\bauto\b"
    "\bbreak\b"
    "\bcase\b"
    "\bchar\b"
    "\bconst\b"
    "\bcontinue\b"
    "\bdefault\b"
    "\bdo\b"
    "\bdouble\b"
    "\belse\b"
    "\benum\b"
    "\bextern\b"
    "\bfloat\b"
    "\bfor\b"
    "\bgoto\b"
    "\bif\b"
    "\binline\b"
    "\bint\b"
    "\blong\b"
    "\bregister\b"
    "\brestrict\b"
    "\bshort\b"
    "\bsigned\b"
    "\bsizeof\b"
    "\bstatic\b"
    "\bstruct\b"
    "\bswitch\b"
    "\btypedef\b"
    "\bunion\b"
    "\bunsigned\b"
    "\bvolatile\b"
    "\bwhile\b"
    "\balignas\b"
    "\balignof\b"
    "\batomic_\w+\b"
    "\batomic_\w+_t\b"
    "\batomic_\w+_\w+_t\b"
    "\bbool\b"
    "\bcomplex\b"
    "\bimaginary\b"
    "\bnoreturn\b"
    "\bstatic_assert\b"
    "\bthread_local\b"
)

DEFAULT_CS_STOP_WORD_ARRAY=(
    "\bint\b"
    "\breturn\b"
    "\bvoid\b"
    "\busing .+;"
    "\busing\b"
    "\bfor\b"
    "\bforeach\b"
    "\bclass\b"
    "\binterface\b"
    "\bstruct\b"
    "\bbool\b"
    "\bstring\b"
    "\bchar\b"
    "\bconst\b"
    "\bprivate\b"
    "\bpublic\b"
    "\binternal\b"
    "\bstatic\b"
    "\breadonly\b"
    "\benum\b"
    "\bsealed\b"
    "\babstract\b"
    "\boverride\b"
    "\bvirtual\b"
    "\bprotected\b"
    "\bif\b"
    "\belse\b"
    "\bswitch\b"
    "\b=>\b"
    "\bnew\b"
    "\bdelegate\b"
    "\bevent\b"
    "\bauto\b"
    "\bvar\b"
    "\bwhile\b"
    "\bnamespace\b"
    "\belseif\b"
    "\bbreak;\b"
    "\breturn;\b"
    "\byield\b"
    "\basync\b"
    "\bawait\b"
    "\bas\b"
    "\bis\b"
    "\bbyte\b"
    "\bcase\b"
    "\bcatch\b"
    "\btry\b"
    "\bcontinue;\b"
    "\bdecimal\b"
    "\bdefault\b"
    "\bdouble\b"
    "\bexplicit\b"
    "\bimplicit\b"
    "\bthrow\b"
    "\bnameof\b"
    "\btrue\b"
    "\bfalse\b"
    "\bin\b"
    "\block\b"
    "\blong\b"
    "\bnull\b"
    "\bobject\b"
    "\boperator\b"
    "\bparams\b"
    "\bout\b"
    "\bref\b"
    "\bsbyte\b"
    "\bshort\b"
    "\bsizeof\b"
    "\bstackalloc\b"
    "\bthis\b"
    "\btypeof\b"
    "\bunit\b"
    "\buclong\b"
    "\bushort\b"
    "\bunsafe\b"
    "\bvolatile\b"
    "\bdo\b"
    "\bbase\b"
    "\bgoto\b"
    "\bget\b"
    "\bset\b"
)

DEFAULT_CPP_STOP_WORD_ARRAY=(
    "\bint\b"
    "\breturn\b"
    "\bvoid\b"
    "#include .*"
    "#pragma .*"
    "#define .*"
    "#if .*"
    "#elif .*"
    "\b#endif\b"
    "#ifdef .*"
    "#ifndef .*"
    "#undef .*"
    "#line .*"
    "#error .*"
    "\bauto\b"
    "\bbreak\b"
    "\bcase\b"
    "\bchar\b"
    "\bconst\b"
    "\bcontinue\b"
    "\bdefault\b"
    "\bdo\b"
    "\bdouble\b"
    "\belse\b"
    "\benum\b"
    "\bextern\b"
    "\bfloat\b"
    "\bfor\b"
    "\bgoto\b"
    "\bif\b"
    "\binline\b"
    "\bint\b"
    "\blong\b"
    "\bregister\b"
    "\brestrict\b"
    "\bshort\b"
    "\bsigned\b"
    "\bsizeof\b"
    "\bstatic\b"
    "\bstruct\b"
    "\bswitch\b"
    "\btypedef\b"
    "\bunion\b"
    "\bunsigned\b"
    "\bvolatile\b"
    "\bwhile\b"
    "\balignas\b"
    "\balignof\b"
    "\batomic_\w+\b"
    "\batomic_\w+_t\b"
    "\batomic_\w+_\w+_t\b"
    "\bbool\b"
    "\bcomplex\b"
    "\bimaginary\b"
    "\bnoreturn\b"
    "\bstatic_assert\b"
    "\bthread_local\b"
    "\basm\b"
    "\bbitand\b"
    "\bbitor\b"
    "\bcatch\b"
    "\bchar8_t\b"
    "\bchar16_t\b"
    "\bchar32_t\b"
    "\bclass\b"
    "\bcompl\b"
    "\bconcept\b"
    "\bconsteval\b"
    "\bconstexpr\b"
    "\bconstinit\b"
    "\bconst_cast\b"
    "\bco_await\b"
    "\bco_return\b"
    "\bco_yield\b"
    "\bdecltype\b"
    "\bdefault\b"
    "\bdelete\b"
    "\bdynamic_cast\b"
    "\bexplicit\b"
    "\bexport\b"
    "\bfloat\b"
    "\bfriend\b"
    "\bmutable\b"
    "\bnamespace\b"
    "\bnew\b"
    "\bnoexcept\b"
    "\bnot\b"
    "\bnullptr\b"
    "\boperator\b"
    "\bor\b"
    "\bprivate\b"
    "\bprotected\b"
    "\bpublic\b"
    "\breflepr\b"
    "\breinterpret_cast\b"
    "\brequires\b"
    "\bstatic_cast\b"
    "\bsynchronized\b"
    "\btemplate\b"
    "\bthis\b"
    "\bthrow\b"
    "\btrue\b"
    "\btry\b"
    "\btypeid\b"
    "\btypename\b"
    "\busing .+;"
    "\busing\b"
    "\bvirtual\b"
    "\bwchar_t\b"
    "\bxor\b"
    "\bfinale\b"
    "\boverride\b"
    "import .*"
    "\bmodule\b"
)

DEFAULT_XML_STOP_WORD_ARRAY=(
    "\w+>"
    "<\w+"
    "\w+\?>"
    "<\?\w+"
)

DEFAULT_JS_STOP_WORD_ARRAY=(
    "\babstract\b"
    "\barguments\b"
    "\bboolean\b"
    "\bbreak\b"
    "\bbyte\b"
    "\bcase\b"
    "\bcatch\b"
    "\bchar\b"
    "\bconst\b"
    "\bcontinue\b"
    "\bdebugger\b"
    "\bdefault\b"
    "\bdelete\b"
    "\bdo\b"
    "\bdouble\b"
    "\belse\b"
    "\beval\b"
    "\bfalse\b"
    "\bfinal\b"
    "\bfinally\b"
    "\bfloat\b"
    "\bfor\b"
    "\bfunction\b"
    "\bgoto\b"
    "\bif\b"
    "\bimplements\b"
    "\bin\b"
    "\binstanceof\b"
    "\bint\b"
    "\binterface\b"
    "\blet\b"
    "\blong\b"
    "\bnative\b"
    "\bnew\b"
    "\bnull\b"
    "\bpackage\b"
    "\bprivate\b"
    "\bprotected\b"
    "\bpublic\b"
    "\breturn\b"
    "\bshort\b"
    "\bstatic\b"
    "\bswitch\b"
    "\bsynchronized\b"
    "\bthis\b"
    "\bthrow\b"
    "\bthrows\b"
    "\btransient\b"
    "\btrue\b"
    "\btry\b"
    "\btyepof\b"
    "\bvar\b"
    "\bvoid\b"
    "\bvolatile\b"
    "\bwhile\b"
    "\bwith\b"
    "\byield\b"
    "\bclass\b"
    "\benum\b"
    "\bexport\b"
    "\bextends\b"
    "import .*"
    "\bsuper\b"
)

DEFAULT_JAVA_STOP_WORD_ARRAY=(
    "\babstract\b"
    "\bassert\b"
    "\bboolean\b"
    "\bbreak\b"
    "\bbyte\b"
    "\bcase\b"
    "\bcatch\b"
    "\bchar\b"
    "\bconst\b"
    "\bcontinue\b"
    "\bdefault\b"
    "\bdelete\b"
    "\bdo\b"
    "\bdouble\b"
    "\belse\b"
    "\bfalse\b"
    "\bfinal\b"
    "\bfinally\b"
    "\bfloat\b"
    "\bfor\b"
    "\bgoto\b"
    "\bif\b"
    "\bimplements\b"
    "\bin\b"
    "\binstanceof\b"
    "\bint\b"
    "\binterface\b"
    "\blong\b"
    "\bmodule\b"
    "\bnative\b"
    "\bnew\b"
    "\bnull\b"
    "\bpackage\b"
    "\bprivate\b"
    "\bprotected\b"
    "\bpublic\b"
    "\breturn\b"
    "\brequires\b"
    "\bshort\b"
    "\bstatic\b"
    "\bswitch\b"
    "\bstrictfp\b"
    "\bsynchronized\b"
    "\bthis\b"
    "\bthrow\b"
    "\bthrows\b"
    "\btransient\b"
    "\btrue\b"
    "\btry\b"
    "\btyepof\b"
    "\bvar\b"
    "\bvoid\b"
    "\bvolatile\b"
    "\bwhile\b"
    "\bwith\b"
    "\bclass\b"
    "\benum\b"
    "\bexports\b"
    "\bextends\b"
    "import .*"
    "\bsuper\b"
)

DEFAULT_RUST_STOP_WORD_ARRAY=(
    "\bas\b"
    "\bbreak\b"
    "\bconst\b"
    "\bcontinue\b"
    "\bcrate\b"
    "\belse\b"
    "\benum\b"
    "\bextern\b"
    "\bfalse\b"
    "\bfn\b"
    "\bfor\b"
    "\bimpl\b"
    "\bin\b"
    "\blet\b"
    "\bloop\b"
    "\bmatch\b"
    "\bmod\b"
    "\bmove\b"
    "\bmut\b"
    "\bpub\b"
    "\bref\b"
    "\breturn\b"
    "\bself\b"
    "\bSelf\b"
    "\bstatic\b"
    "\bstruct\b"
    "\bsuper\b"
    "\btrait\b"
    "\btrue\b"
    "\btype\b"
    "\bunsafe\b"
    "\buse .*"
    "\bwhere\b"
    "\bwhile\b"
    "\basync\b"
    "\bawait\b"
    "\bdyn\b"
    "\babstract\b"
    "\bbecome\b"
    "\bbox\b"
    "\bdo\b"
    "\bfinal\b"
    "\bmacro\b"
    "\boverride\b"
    "\bpriv\b"
    "\btypeof\b"
    "\bunsized\b"
    "\bvirtual\b"
    "\byield\b"
    "\btry\b"
    "\bunion\b"
    "\b'static\b"
)

DEFAULT_STOP_WORD_ARRAY=(
    "${DEFAULT_C_STOP_WORD_ARRAY[@]}"
    "${DEFAULT_CS_STOP_WORD_ARRAY[@]}"
    "${DEFAULT_CPP_STOP_WORD_ARRAY[@]}"
    "${DEFAULT_JS_STOP_WORD_ARRAY[@]}"
    "${DEFAULT_JAVA_STOP_WORD_ARRAY[@]}"
    "${DEFAULT_RUST_STOP_WORD_ARRAY[@]}"
    "${DEFAULT_XML_STOP_WORD_ARRAY[@]}"
)

# shellcheck disable=SC2034
LEVEL1_CMD=(
    "sed -e 's/./X/g'"
)

# shellcheck disable=SC2034
LEVEL2_CMD=(
    "sed -e 's/\S/X/g'"
)

# shellcheck disable=SC2034
LEVEL3_CMD=(
    "sed -e 's/[a-z]/x/g' -e 's/[A-Z]/X/g'"
)

# shellcheck disable=SC2034,SC2016
LEVEL4_CMD=(
    'sed -E "s/(${STOP_WORD_LIST})/\n${STOP_WORD_MARKER}&\n${STOP_WORD_MARKER}/g"'
    '| sed -E "/(${STOP_WORD_LIST})/!s/[a-z]/x/g"'
    '| sed -E "/(${STOP_WORD_LIST})/!s/[A-Z]/x/g"'
    '| sed ":a;N;\$!ba;s/\n${STOP_WORD_MARKER}//g"'
)

# shellcheck disable=SC2034,SC2016
LEVEL5_CMD=(
    'sed -E "s/(${STOP_WORD_LIST})/\n${STOP_WORD_MARKER}&\n${STOP_WORD_MARKER}/g"'
    '| sed -E "/(${STOP_WORD_LIST})/!s/[a-z]/x/g"'
    '| sed ":a;N;\$!ba;s/\n${STOP_WORD_MARKER}//g"'
)

USAGE_MESSAGE="Usage: $0 <FILE_PATH> [CUSTOM_STOP_WORD_FILE]... [OPTION]...

OPTION:

    ** stop word lists (if no one is given, a stop word list based on file extension will be used)
    --no-defaults - disable default stop word list
    --cs - use default stop word list for csharp
    --c - use default stop word list for c
    --cpp - use default stop word list for cpp
    --js - use default stop word list for javascript
    --java - use default stop word list for java
    --rs - use default stop word list for rust
    --xml - use default stop word list for xml
    --all - use all default stop word lists
    
    ** in/output settings
    --quiet - do not print additional output like level number
    --non-interactive - disable user interaction like read and clear

    ** all five level will be printed by default, except one of following options is used
    --level1 - show level 1
    --level2 - show level 2
    --level3 - show level 3
    --level4 - show level 4
    --level5 - show level 5
    --skip-solution - do not show the solution
"

ARGS=("$@")
if [ -p /dev/stdin ]; then
    CONTENT=$(cat | sed -e 's/\t/    /g' | sed -e 's/[[:space:]]*$//')
    ARGS+=("--non-interactive")
else

    if [ $# -lt 1 ]; then
        echo "${USAGE_MESSAGE}"
        exit 1
    fi

    CONTENT=$(sed -e 's/\t/    /g' "$1" | sed -e 's/[[:space:]]*$//')
    FILE_EXTENSION="${1##*.}"
    shift
    ARGS=("$@")
    if [[ ! " ${ARGS[*]} " =~ "--no-defaults" ]]; then
        case "$FILE_EXTENSION" in
            cs)
                ARGS+=("--cs")
            ;;
            c)
                ARGS+=("--c")
            ;;
            cpp)
                ARGS+=("--cpp")
            ;;
            js)
                ARGS+=("--js")
            ;;
            java)
                ARGS+=("--java")
            ;;
            rs)
                ARGS+=("--rs")
            ;;
            xml)
                ARGS+=("--xml")
            ;;
            *)
            ;;
        esac
    fi
fi

STOP_WORD_ARRAY=()
LEVEL_ARRAY=()
OPTIONAL_ECHO_CMD="echo"
READ_CMD="read"
CLEAR_CMD='printf "\033c'

for ARG in "${ARGS[@]}"; do
    case "$ARG" in
        --cs)
            STOP_WORD_ARRAY=(
                "${STOP_WORD_ARRAY[@]}"
                "${DEFAULT_CS_STOP_WORD_ARRAY[@]}"
            )
        ;;
        --c)
            STOP_WORD_ARRAY=(
                "${STOP_WORD_ARRAY[@]}"
                "${DEFAULT_C_STOP_WORD_ARRAY[@]}"
            )
        ;;
        --cpp)
            STOP_WORD_ARRAY=(
                "${STOP_WORD_ARRAY[@]}"
                "${DEFAULT_CPP_STOP_WORD_ARRAY[@]}"
            )
        ;;
        --js)
            STOP_WORD_ARRAY=(
                "${STOP_WORD_ARRAY[@]}"
                "${DEFAULT_JS_STOP_WORD_ARRAY[@]}"
            )
        ;;
        --java)
            STOP_WORD_ARRAY=(
                "${STOP_WORD_ARRAY[@]}"
                "${DEFAULT_JAVA_STOP_WORD_ARRAY[@]}"
            )
        ;;
        --rs)
            STOP_WORD_ARRAY=(
                "${STOP_WORD_ARRAY[@]}"
                "${DEFAULT_RUST_STOP_WORD_ARRAY[@]}"
            )
        ;;
        --xml)
            STOP_WORD_ARRAY=(
                "${STOP_WORD_ARRAY[@]}"
                "${DEFAULT_XML_WORD_ARRAY[@]}"
            )
        ;;
        --all)
            STOP_WORD_ARRAY=(
                "${DEFAULT_STOP_WORD_ARRAY[@]}"
            )
        ;;
        --quiet)
            OPTIONAL_ECHO_CMD="true || "
        ;;
        --level1)
            LEVEL_ARRAY+=("1")
        ;;
        --level2)
            LEVEL_ARRAY+=("2")
        ;;
        --level3)
            LEVEL_ARRAY+=("3")
        ;;
        --level4)
            LEVEL_ARRAY+=("4")
        ;;
        --level5)
            LEVEL_ARRAY+=("5")
        ;;
        --non-interactive)
            READ_CMD="true || "
            CLEAR_CMD="true || "
        ;;
        --skip-solution)
            SKIP_SOLUTION="y"
        ;;
        *)
            readarray -t CUSTOM_STOP_WORD_ARRAY <"$ARG"
            STOP_WORD_ARRAY=(
                "${STOP_WORD_ARRAY[@]}"
                "${CUSTOM_STOP_WORD_ARRAY[@]}"
            )
        ;;
    esac
done

if [ ${#STOP_WORD_ARRAY[@]} -eq 0 ]; then
   STOP_WORD_ARRAY=(
        "${DEFAULT_STOP_WORD_ARRAY[@]}"
    )
fi

if [ ${#LEVEL_ARRAY[@]} -eq 0 ]; then
   LEVEL_ARRAY=(
        "1"
        "2"
        "3"
        "4"
        "5"
    )
fi

# shellcheck disable=SC2034
STOP_WORD_LIST=$( IFS=$'|'; echo "(${STOP_WORD_LINE_MARKER})*${STOP_WORD_ARRAY[*]}" )

SHOW_LEVEL()
{
    $CLEAR_CMD
    $OPTIONAL_ECHO_CMD "### LEVEL ${LEVEL} ###"
    LEVEL_CMD="LEVEL${LEVEL}_CMD"
    echo "${CONTENT}" | eval "eval \"\${${LEVEL_CMD}[@]}\""
    $OPTIONAL_ECHO_CMD "###############"
}

for LEVEL in "${LEVEL_ARRAY[@]::${#LEVEL_ARRAY[@]}-1}"; do
    SHOW_LEVEL
    $READ_CMD -p  "press enter for next level"
done

LEVEL=${LEVEL_ARRAY[-1]}
SHOW_LEVEL

if [ -v SKIP_SOLUTION ]; then
    exit 0
fi

$READ_CMD -p  "press enter for solution"

$CLEAR_CMD
$OPTIONAL_ECHO_CMD  "### SOLUTION ###"
echo "${CONTENT}"
$OPTIONAL_ECHO_CMD  "###############"
